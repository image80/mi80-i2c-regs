#pragma once

#include "mi80_common_regs.h"

#define MI80_104_DEFAULT_I2C_ADDRESS 0x44

typedef enum Mi80_104_Int_Flags_enum {
  MI80_104_BUTTON1_RELEASE_INT_FLAG = 0x01,
  MI80_104_BUTTON1_PRESS_INT_FLAG = 0x02,
  MI80_104_BUTTON2_RELEASE_INT_FLAG = 0x04,
  MI80_104_BUTTON2_PRESS_INT_FLAG = 0x08,
  MI80_104_BUTTON3_RELEASE_INT_FLAG = 0x10,
  MI80_104_BUTTON3_PRESS_INT_FLAG = 0x20,

  MI80_104_IO1_INT_RISE_FLAG = 0x40,
  MI80_104_IO1_INT_FALL_FLAG = 0x80,
  MI80_104_IO2_INT_RISE_FLAG = 0x100,
  MI80_104_IO2_INT_FALL_FLAG = 0x200,
  MI80_104_IO3_INT_RISE_FLAG = 0x400,
  MI80_104_IO3_INT_FALL_FLAG = 0x800,
  MI80_104_IO4_INT_RISE_FLAG = 0x1000,
  MI80_104_IO4_INT_FALL_FLAG = 0x2000,
} Mi80_104_Int_Flags;

typedef enum Mi80_104_LedsPattern_enum {
  MI80_104_LEDS_PATTERN_OFF = 0,
  MI80_104_LEDS_PATTERN_SHIFT = 1,
  MI80_104_LEDS_PATTERN_RAINBOW = 2,
  MI80_104_LEDS_PATTERN_POLICE = 3,
  MI80_104_LEDS_PATTERN_PULSE_RED = 4,
  MI80_104_LEDS_PATTERN_PULSE_GREEN = 5,
  MI80_104_LEDS_PATTERN_PULSE_BLUE = 6,
  MI80_104_LEDS_PATTERN_PULSE_YELLOW = 7,
  MI80_104_LEDS_PATTERN_PULSE_CYAN = 8,
  MI80_104_LEDS_PATTERN_PULSE_MAGENTA = 9,
  MI80_104_LEDS_PATTERN_CUSTOM = 10,
  MI80_104_LEDS_PATTERN_CUSTOM_HUE = 11,
} Mi80_104_LedsPattern;

typedef enum Mi80_104_ResetFlags_enum {
  MI80_104_RESET_RESTORE_PIO = 0x01,
  MI80_104_RESET_RESTORE_BUTTONS = 0x02,
  MI80_104_RESET_RESTORE_I2C = 0x04,
  MI80_104_RESET_RESTORE_ALL = 0x07
} Mi80_104_ResetFlags;

typedef enum Mi80_104_Register_enum {
  MI80_104_REG_RGB_LEDS = 0x00,
  MI80_104_REG_RGB_LEDS_HSV = 0x01,
  MI80_104_REG_LEDS_PATTERN = 0x02,
  MI80_104_REG_LEDS_PATTERN_MAX_VALUE = 0x03,

  MI80_104_REG_RGB_LED_HSV_BASE = 0x10,

} Mi80_104_Register;

///////////////////////////////////////////////////////////////////////////////////
// RGB LEDS

#define MI80_104_REG_RGB_LEDS_PATTERN_DEF \
  { MI80_104_REG_LEDS_PATTERN, I2C_DATA_TYPE_UINT8, 1, MI80_REG_RW }

#define MI80_104_REG_RGB_LEDS_PATTERN_INFO \
  { "rgbPattern", "RGB Pattern", "Set or get RGB pattern", MI80_104_REG_RGB_LEDS_PATTERN_DEF }

#define MI80_104_REG_RGB_LEDS_PATTERN_MAX_VALUE_DEF \
  { MI80_104_REG_LEDS_PATTERN_MAX_VALUE, I2C_DATA_TYPE_UINT8, 1, MI80_REG_RW }

#define MI80_104_REG_RGB_LEDS_PATTERN_MAX_VALUE_INFO \
  { "rgbMaxValue", "Pattern Max Value", "Set or get max value (led brightness) for patterns", MI80_104_REG_RGB_LEDS_PATTERN_MAX_VALUE_DEF }

#define MI80_104_REG_RGB_LEDS_DEF \
  { MI80_104_REG_RGB_LEDS, I2C_DATA_TYPE_RGB, 9, MI80_REG_RW }

#define MI80_104_REG_RGB_LEDS_INFO \
  { "rgbLeds", "RGB Leds", "Set values of all 9 RGB Leds", MI80_104_REG_RGB_LEDS_DEF }

#define MI80_104_REG_RGB_LED_1 MI80_REG_RGB_LED_BASE
#define MI80_104_REG_RGB_LED_2 MI80_REG_RGB_LED_BASE + 1
#define MI80_104_REG_RGB_LED_3 MI80_REG_RGB_LED_BASE + 2
#define MI80_104_REG_RGB_LED_4 MI80_REG_RGB_LED_BASE + 3
#define MI80_104_REG_RGB_LED_5 MI80_REG_RGB_LED_BASE + 4
#define MI80_104_REG_RGB_LED_6 MI80_REG_RGB_LED_BASE + 5
#define MI80_104_REG_RGB_LED_7 MI80_REG_RGB_LED_BASE + 6
#define MI80_104_REG_RGB_LED_8 MI80_REG_RGB_LED_BASE + 7
#define MI80_104_REG_RGB_LED_9 MI80_REG_RGB_LED_BASE + 8

#define MI80_104_REG_RGB_LED_1_DEF \
  { MI80_104_REG_RGB_LED_1, I2C_DATA_TYPE_RGB, 1, MI80_REG_RW }
#define MI80_104_REG_RGB_LED_2_DEF \
  { MI80_104_REG_RGB_LED_2, I2C_DATA_TYPE_RGB, 1, MI80_REG_RW }
#define MI80_104_REG_RGB_LED_3_DEF \
  { MI80_104_REG_RGB_LED_3, I2C_DATA_TYPE_RGB, 1, MI80_REG_RW }
#define MI80_104_REG_RGB_LED_4_DEF \
  { MI80_104_REG_RGB_LED_4, I2C_DATA_TYPE_RGB, 1, MI80_REG_RW }
#define MI80_104_REG_RGB_LED_5_DEF \
  { MI80_104_REG_RGB_LED_5, I2C_DATA_TYPE_RGB, 1, MI80_REG_RW }
#define MI80_104_REG_RGB_LED_6_DEF \
  { MI80_104_REG_RGB_LED_6, I2C_DATA_TYPE_RGB, 1, MI80_REG_RW }
#define MI80_104_REG_RGB_LED_7_DEF \
  { MI80_104_REG_RGB_LED_7, I2C_DATA_TYPE_RGB, 1, MI80_REG_RW }
#define MI80_104_REG_RGB_LED_8_DEF \
  { MI80_104_REG_RGB_LED_8, I2C_DATA_TYPE_RGB, 1, MI80_REG_RW }
#define MI80_104_REG_RGB_LED_9_DEF \
  { MI80_104_REG_RGB_LED_9, I2C_DATA_TYPE_RGB, 1, MI80_REG_RW }

#define MI80_104_REG_RGB_LED_1_INFO \
  { "rgbLed1", "RGB Led 1", "Get or set value of RGB Led #1", MI80_104_REG_RGB_LED_1_DEF }
#define MI80_104_REG_RGB_LED_2_INFO \
  { "rgbLed2", "RGB Led 2", "Get or set value of RGB Led #2", MI80_104_REG_RGB_LED_2_DEF }
#define MI80_104_REG_RGB_LED_3_INFO \
  { "rgbLed3", "RGB Led 3", "Get or set value of RGB Led #3", MI80_104_REG_RGB_LED_3_DEF }
#define MI80_104_REG_RGB_LED_4_INFO \
  { "rgbLed4", "RGB Led 4", "Get or set value of RGB Led #4", MI80_104_REG_RGB_LED_4_DEF }
#define MI80_104_REG_RGB_LED_5_INFO \
  { "rgbLed5", "RGB Led 5", "Get or set value of RGB Led #5", MI80_104_REG_RGB_LED_5_DEF }
#define MI80_104_REG_RGB_LED_6_INFO \
  { "rgbLed6", "RGB Led 6", "Get or set value of RGB Led #6", MI80_104_REG_RGB_LED_6_DEF }
#define MI80_104_REG_RGB_LED_7_INFO \
  { "rgbLed7", "RGB Led 7", "Get or set value of RGB Led #7", MI80_104_REG_RGB_LED_7_DEF }
#define MI80_104_REG_RGB_LED_8_INFO \
  { "rgbLed8", "RGB Led 8", "Get or set value of RGB Led #8", MI80_104_REG_RGB_LED_8_DEF }
#define MI80_104_REG_RGB_LED_9_INFO \
  { "rgbLed9", "RGB Led 9", "Get or set value of RGB Led #9", MI80_104_REG_RGB_LED_9_DEF }

///////////////////////////////////////////////////////////////////////////////////
// RGB LEDS HSV

#define MI80_104_REG_RGB_LEDS_HSV_DEF \
  { MI80_104_REG_RGB_LEDS_HSV, I2C_DATA_TYPE_HSV, 9, MI80_REG_RW }

#define MI80_104_REG_RGB_LEDS_HSV_INFO \
  { "hsvLeds", "HSV Leds", "Set HSV values of all 9 RGB Leds", MI80_104_REG_RGB_LEDS_HSV_DEF }

#define MI80_104_REG_RGB_LED_HSV_1 MI80_104_REG_RGB_LED_HSV_BASE
#define MI80_104_REG_RGB_LED_HSV_2 MI80_104_REG_RGB_LED_HSV_BASE + 1
#define MI80_104_REG_RGB_LED_HSV_3 MI80_104_REG_RGB_LED_HSV_BASE + 2
#define MI80_104_REG_RGB_LED_HSV_4 MI80_104_REG_RGB_LED_HSV_BASE + 3
#define MI80_104_REG_RGB_LED_HSV_5 MI80_104_REG_RGB_LED_HSV_BASE + 4
#define MI80_104_REG_RGB_LED_HSV_6 MI80_104_REG_RGB_LED_HSV_BASE + 5
#define MI80_104_REG_RGB_LED_HSV_7 MI80_104_REG_RGB_LED_HSV_BASE + 6
#define MI80_104_REG_RGB_LED_HSV_8 MI80_104_REG_RGB_LED_HSV_BASE + 7
#define MI80_104_REG_RGB_LED_HSV_9 MI80_104_REG_RGB_LED_HSV_BASE + 8

#define MI80_104_REG_RGB_LED_HSV_1_DEF \
  { MI80_104_REG_RGB_LED_HSV_1, I2C_DATA_TYPE_HSV, 1, MI80_REG_RW }
#define MI80_104_REG_RGB_LED_HSV_2_DEF \
  { MI80_104_REG_RGB_LED_HSV_2, I2C_DATA_TYPE_HSV, 1, MI80_REG_RW }
#define MI80_104_REG_RGB_LED_HSV_3_DEF \
  { MI80_104_REG_RGB_LED_HSV_3, I2C_DATA_TYPE_HSV, 1, MI80_REG_RW }
#define MI80_104_REG_RGB_LED_HSV_4_DEF \
  { MI80_104_REG_RGB_LED_HSV_4, I2C_DATA_TYPE_HSV, 1, MI80_REG_RW }
#define MI80_104_REG_RGB_LED_HSV_5_DEF \
  { MI80_104_REG_RGB_LED_HSV_5, I2C_DATA_TYPE_HSV, 1, MI80_REG_RW }
#define MI80_104_REG_RGB_LED_HSV_6_DEF \
  { MI80_104_REG_RGB_LED_HSV_6, I2C_DATA_TYPE_HSV, 1, MI80_REG_RW }
#define MI80_104_REG_RGB_LED_HSV_7_DEF \
  { MI80_104_REG_RGB_LED_HSV_7, I2C_DATA_TYPE_HSV, 1, MI80_REG_RW }
#define MI80_104_REG_RGB_LED_HSV_8_DEF \
  { MI80_104_REG_RGB_LED_HSV_8, I2C_DATA_TYPE_HSV, 1, MI80_REG_RW }
#define MI80_104_REG_RGB_LED_HSV_9_DEF \
  { MI80_104_REG_RGB_LED_HSV_9, I2C_DATA_TYPE_HSV, 1, MI80_REG_RW }

#define MI80_104_REG_RGB_LED_HSV_1_INFO \
  { "hsvLed1", "HSV Led 1", "Set HSV value of RGB Led #1", MI80_104_REG_RGB_LED_HSV_1_DEF }
#define MI80_104_REG_RGB_LED_HSV_2_INFO \
  { "hsvLed2", "HSV Led 2", "Set HSV value of RGB Led #2", MI80_104_REG_RGB_LED_HSV_2_DEF }
#define MI80_104_REG_RGB_LED_HSV_3_INFO \
  { "hsvLed3", "HSV Led 3", "Set HSV value of RGB Led #3", MI80_104_REG_RGB_LED_HSV_3_DEF }
#define MI80_104_REG_RGB_LED_HSV_4_INFO \
  { "hsvLed4", "HSV Led 4", "Set HSV value of RGB Led #4", MI80_104_REG_RGB_LED_HSV_4_DEF }
#define MI80_104_REG_RGB_LED_HSV_5_INFO \
  { "hsvLed5", "HSV Led 5", "Set HSV value of RGB Led #5", MI80_104_REG_RGB_LED_HSV_5_DEF }
#define MI80_104_REG_RGB_LED_HSV_6_INFO \
  { "hsvLed6", "HSV Led 6", "Set HSV value of RGB Led #6", MI80_104_REG_RGB_LED_HSV_6_DEF }
#define MI80_104_REG_RGB_LED_HSV_7_INFO \
  { "hsvLed7", "HSV Led 7", "Set HSV value of RGB Led #7", MI80_104_REG_RGB_LED_HSV_7_DEF }
#define MI80_104_REG_RGB_LED_HSV_8_INFO \
  { "hsvLed8", "HSV Led 8", "Set HSV value of RGB Led #8", MI80_104_REG_RGB_LED_HSV_8_DEF }
#define MI80_104_REG_RGB_LED_HSV_9_INFO \
  { "hsvLed9", "HSV Led 9", "Set HSV value of RGB Led #9", MI80_104_REG_RGB_LED_HSV_9_DEF }

#define MI80_104_I2C_REG_INFO                              \
  {                                                        \
    MI80_104_REG_RGB_LEDS_INFO,                       /**/ \
        MI80_104_REG_RGB_LED_1_INFO,                  /**/ \
        MI80_104_REG_RGB_LED_2_INFO,                  /**/ \
        MI80_104_REG_RGB_LED_3_INFO,                  /**/ \
        MI80_104_REG_RGB_LED_4_INFO,                  /**/ \
        MI80_104_REG_RGB_LED_5_INFO,                  /**/ \
        MI80_104_REG_RGB_LED_6_INFO,                  /**/ \
        MI80_104_REG_RGB_LED_7_INFO,                  /**/ \
        MI80_104_REG_RGB_LED_8_INFO,                  /**/ \
        MI80_104_REG_RGB_LED_9_INFO,                  /**/ \
        MI80_104_REG_RGB_LEDS_HSV_INFO,               /**/ \
        MI80_104_REG_RGB_LED_HSV_1_INFO,              /**/ \
        MI80_104_REG_RGB_LED_HSV_2_INFO,              /**/ \
        MI80_104_REG_RGB_LED_HSV_3_INFO,              /**/ \
        MI80_104_REG_RGB_LED_HSV_4_INFO,              /**/ \
        MI80_104_REG_RGB_LED_HSV_5_INFO,              /**/ \
        MI80_104_REG_RGB_LED_HSV_6_INFO,              /**/ \
        MI80_104_REG_RGB_LED_HSV_7_INFO,              /**/ \
        MI80_104_REG_RGB_LED_HSV_8_INFO,              /**/ \
        MI80_104_REG_RGB_LED_HSV_9_INFO,              /**/ \
        MI80_104_REG_RGB_LEDS_PATTERN_INFO,           /**/ \
        MI80_104_REG_RGB_LEDS_PATTERN_MAX_VALUE_INFO, /**/ \
        MI80_REG_IO_1_MODE_INFO,                      /**/ \
        MI80_REG_IO_2_MODE_INFO,                      /**/ \
        MI80_REG_IO_3_MODE_INFO,                      /**/ \
        MI80_REG_IO_4_MODE_INFO,                      /**/ \
        MI80_REG_IO_1_INFO,                           /**/ \
        MI80_REG_IO_2_INFO,                           /**/ \
        MI80_REG_IO_3_INFO,                           /**/ \
        MI80_REG_IO_4_INFO,                           /**/ \
        MI80_REG_I2C_ADRRESS_INFO,                    /**/ \
        MI80_REG_MODULE_ID_INFO,                      /**/ \
        MI80_REG_MODULE_INFO_INFO,                    /**/ \
        MI80_REG_MODULE_DESCRIPTION_INFO,             /**/ \
        MI80_REG_INT_ENABLE_INFO,                     /**/ \
        MI80_REG_INT_FLAGS_INFO,                      /**/ \
        MI80_CMD_RESET_INFO,                          /**/ \
        MI80_CMD_PERSIST_CONFIG_INFO,                 /**/ \
        MI80_REG_SINT_GPIO_INFO,                      /**/ \
        {NULL},                                       /**/ \
  }

#define MI80_104_INTERRUPT_INFO                                                                                   \
  {                                                                                                               \
    {MI80_104_BUTTON1_PRESS_INT_FLAG, "button1Press", "Button 1 Press", "Button 1 was pressed"},             /**/ \
        {MI80_104_BUTTON1_RELEASE_INT_FLAG, "button1Release", "Button 1 Released", "Button 1 was released"}, /**/ \
        {MI80_104_BUTTON2_PRESS_INT_FLAG, "button2Press", "Button 2 Press", "Button 2 was pressed"},         /**/ \
        {MI80_104_BUTTON2_RELEASE_INT_FLAG, "button2Release", "Button 2 Released", "Button 2 was released"}, /**/ \
        {MI80_104_BUTTON3_PRESS_INT_FLAG, "button3Press", "Button 3 Press", "Button 3 was pressed"},         /**/ \
        {MI80_104_BUTTON3_RELEASE_INT_FLAG, "button3Release", "Button 3 Released", "Button 3 was released"}, /**/ \
        {MI80_104_IO1_INT_RISE_FLAG, "io1Rise", "IO 1 Rise", "IO #1 rising edge"},                           /**/ \
        {MI80_104_IO1_INT_FALL_FLAG, "io1Fall", "IO 1 Fall", "IO #1 falling edge"},                          /**/ \
        {MI80_104_IO2_INT_RISE_FLAG, "io2Rise", "IO 2 Rise", "IO #2 rising edge"},                           /**/ \
        {MI80_104_IO2_INT_FALL_FLAG, "io2Fall", "IO 2 Fall", "IO #2 falling edge"},                          /**/ \
        {MI80_104_IO3_INT_RISE_FLAG, "io3Rise", "IO 3 Rise", "IO #3 rising edge"},                           /**/ \
        {MI80_104_IO3_INT_FALL_FLAG, "io3Fall", "IO 3 Fall", "IO #3 falling edge"},                          /**/ \
        {MI80_104_IO4_INT_RISE_FLAG, "io4Rise", "IO 4 Rise", "IO #4 rising edge"},                           /**/ \
        {MI80_104_IO4_INT_FALL_FLAG, "io4Fall", "IO 4 Fall", "IO #4 falling edge"},                          /**/ \
    {                                                                                                             \
      0                                                                                                           \
    } /**/                                                                                                        \
  }

#define MI80_104_I2C_MODULE_INFO \
  { MI80_104_DEFAULT_I2C_ADDRESS, 104, "Mi80-104", "Mi80-104", "Mi80-104 RGB Led array", MI80_104_I2C_REG_INFO, MI80_104_INTERRUPT_INFO }
