#ifndef _MI80_115_REGS_H_
#define _MI80_115_REGS_H_

#include "mi80_common_regs.h"

#define MI80_115_DEFAULT_I2C_ADDRESS 0x4f
#define MI80_115_MODULE_ID 115
#define MI80_115_MODULE_NAME "Mi80-115"
#define MI80_115_MODULE_FEATURES HAS_STATUS_LED
#define MI80_115_MODULE_DESCRIPTION "Mi80-115 Stepper Driver"

#define MI80_115_NMBR_RGB_LEDS 0
#define MI80_115_NMBR_BUTTONS 0
#define MI80_115_NMBR_IO 0

typedef enum MI80_115_Int_Flags_enum {
  MI80_115_LIMIT1_INT_RISE_FLAG = 0x01,
  MI80_115_LIMIT1_INT_FALL_FLAG = 0x02,
  MI80_115_LIMI2_INT_RISE_FLAG = 0x04,
  MI80_115_LIMI2_INT_FALL_FLAG = 0x08,
} MI80_115_Int_Flags;

typedef enum MI80_115_Register_enum {
  MI80_115_REG_DRV_ENABLE = 0x00,
  MI80_115_REG_DRV_DUTY_CYCLE = 0x01,
  MI80_115_REG_ADC_ON_VAL = 0x02,
  MI80_115_REG_ADC_OFF_VAL = 0x03,
  MI80_115_REG_ADC_SHUNT_VAL = 0x04,
  MI80_115_REG_DRV_ON_MV = 0x05,
  MI80_115_REG_DRV_OFF_MV = 0x06,
  MI80_115_REG_DRV_SHUNT_MV = 0x07,
  MI80_115_REG_DRV_CURRENT_MA = 0x08,

} MI80_115_Register;

#define MI80_115_REG_DRV_FREQ_DEF \
  { MI80_115_REG_DRV_FREQ, I2C_DATA_TYPE_UINT16, 1, MI80_REG_RW }
#define MI80_115_REG_DRV_DUTY_CYCLE_DEF \
  { MI80_115_REG_DRV_DUTY_CYCLE, I2C_DATA_TYPE_UINT8, 1, MI80_REG_RW }
#define MI80_115_REG_ADC_ON_VAL_DEF \
  { MI80_115_REG_ADC_ON_VAL, I2C_DATA_TYPE_UINT16, 1, MI80_REG_RO }
#define MI80_115_REG_ADC_OFF_VAL_DEF \
  { MI80_115_REG_ADC_ON_VAL, I2C_DATA_TYPE_UINT16, 1, MI80_REG_RO }
#define MI80_115_REG_ADC_SHUNT_VAL_DEF \
  { MI80_115_REG_ADC_SHUNT_VAL, I2C_DATA_TYPE_UINT16, 1, MI80_REG_RO }
#define MI80_115_REG_DRV_ON_MV_DEF \
  { MI80_115_REG_DRV_ON_MV, I2C_DATA_TYPE_UINT16, 1, MI80_REG_RO }
#define MI80_115_REG_DRV_OFF_MV_DEF \
  { MI80_115_REG_DRV_OFF_MV, I2C_DATA_TYPE_UINT16, 1, MI80_REG_RO }
#define MI80_115_REG_DRV_SHUNT_MV_DEF \
  { MI80_115_REG_DRV_SHUNT_MV, I2C_DATA_TYPE_UINT16, 1, MI80_REG_RO }
#define MI80_115_REG_DRV_CURRENT_MA_DEF \
  { MI80_115_REG_DRV_CURRENT_MA, I2C_DATA_TYPE_UINT16, 1, MI80_REG_RO }
#define MI80_115_REG_DRV_ENABLE_DEF \
  { MI80_115_REG_DRV_ENABLE, I2C_DATA_TYPE_UINT8, 1, MI80_REG_RW }

#define MI80_115_REG_DRV_FREQ_INFO \
  { "drvFreq", "Driver Frequency", "Get or set driver frequency", MI80_115_REG_DRV_FREQ_DEF }
#define MI80_115_REG_DRV_DUTY_CYCLE_INFO \
  { "drvDuty", "Drive Duty Cycle", "Get or set driver duty cycle", MI80_115_REG_DRV_DUTY_CYCLE_DEF }
#define MI80_115_REG_ADC_ON_VAL_INFO \
  { "drvAdcOn", "Driver ON ADC", "Get driver ON ADC value", MI80_115_REG_ADC_ON_VAL_DEF }
#define MI80_115_REG_ADC_OFF_VAL_INFO \
  { "drvAdcOff", "Driver OFF ADC", "Get driver OFF ADC value", MI80_115_REG_ADC_OFF_VAL_DEF }
#define MI80_115_REG_ADC_SHUNT_VAL_INFO \
  { "shuntAdc", "Shunt ADC", "Get shunt ADC value", MI80_115_REG_ADC_SHUNT_VAL_DEF }
#define MI80_115_REG_DRV_ON_MV_INFO \
  { "drvMvOn", "Driver ON MV", "Get driver ON voltage in MV", MI80_115_REG_DRV_ON_MV_DEF }
#define MI80_115_REG_DRV_OFF_MV_INFO \
  { "drvMvOff", "Driver OFF MV", "Get driver OFF voltage in MV", MI80_115_REG_DRV_OFF_MV_DEF }
#define MI80_115_REG_DRV_SHUNT_MV_INFO \
  { "shuntMv", "Shunt MV", "Get shunt voltage in MV", MI80_115_REG_DRV_SHUNT_MV_DEF }
#define MI80_115_REG_DRV_ENABLE_INFO \
  { "drvEnable", "Driver Enable", "Enable driver output", MI80_115_REG_DRV_ENABLE_DEF }

#define MI80_115_REG_IO_1_MODE MI80_REG_IO_MODE_BASE
#define MI80_115_REG_IO_2_MODE MI80_REG_IO_MODE_BASE + 1
#define MI80_115_REG_IO_3_MODE MI80_REG_IO_MODE_BASE + 2
#define MI80_115_REG_IO_4_MODE MI80_REG_IO_MODE_BASE + 3

#define MI80_115_REG_IO_1_MODE_DEF \
  { MI80_115_REG_IO_1_MODE, I2C_DATA_TYPE_UINT8, 6, MI80_REG_RW }
#define MI80_115_REG_IO_2_MODE_DEF \
  { MI80_115_REG_IO_2_MODE, I2C_DATA_TYPE_UINT8, 6, MI80_REG_RW }
#define MI80_115_REG_IO_3_MODE_DEF \
  { MI80_115_REG_IO_3_MODE, I2C_DATA_TYPE_UINT8, 6, MI80_REG_RW }
#define MI80_115_REG_IO_4_MODE_DEF \
  { MI80_115_REG_IO_4_MODE, I2C_DATA_TYPE_UINT8, 6, MI80_REG_RW }

#define MI80_115_REG_IO_1_MODE_INFO \
  { "ioMode1", "IO 1 Mode", "Get or set mode of IO #1", MI80_115_REG_IO_1_MODE_DEF }
#define MI80_115_REG_IO_2_MODE_INFO \
  { "ioMode2", "IO 2 Mode", "Get or set mode of IO #2", MI80_115_REG_IO_2_MODE_DEF }
#define MI80_115_REG_IO_3_MODE_INFO \
  { "ioMode3", "IO 3 Mode", "Get or set mode of IO #3", MI80_115_REG_IO_3_MODE_DEF }
#define MI80_115_REG_IO_4_MODE_INFO \
  { "ioMode4", "IO 4 Mode", "Get or set mode of IO #4", MI80_115_REG_IO_4_MODE_DEF }

#define MI80_115_REG_IO_1 MI80_REG_IO_BASE
#define MI80_115_REG_IO_2 MI80_REG_IO_BASE + 1
#define MI80_115_REG_IO_3 MI80_REG_IO_BASE + 2
#define MI80_115_REG_IO_4 MI80_REG_IO_BASE + 3

#define MI80_115_REG_IO_1_DEF \
  { MI80_115_REG_IO_1, I2C_DATA_TYPE_UINT8, 1, MI80_REG_RW }
#define MI80_115_REG_IO_2_DEF \
  { MI80_115_REG_IO_2, I2C_DATA_TYPE_UINT8, 1, MI80_REG_RW }
#define MI80_115_REG_IO_3_DEF \
  { MI80_115_REG_IO_3, I2C_DATA_TYPE_UINT8, 1, MI80_REG_RW }
#define MI80_115_REG_IO_4_DEF \
  { MI80_115_REG_IO_4, I2C_DATA_TYPE_UINT8, 1, MI80_REG_RW }

#define MI80_115_REG_IO_1_INFO \
  { "io1", "IO 1", "Get or set value of IO #1", MI80_115_REG_IO_1_DEF }
#define MI80_115_REG_IO_2_INFO \
  { "io2", "IO 2", "Get or set value of IO #2", MI80_115_REG_IO_2_DEF }
#define MI80_115_REG_IO_3_INFO \
  { "io3", "IO 3", "Get or set value of IO #3", MI80_115_REG_IO_3_DEF }
#define MI80_115_REG_IO_4_INFO \
  { "io4", "IO 4", "Get or set value of IO #4", MI80_115_REG_IO_4_DEF }

///////////////////////////////////////////////////////////////////////////////////////////

#define MI80_115_REGS_I2C_INFO               \
  MI80_115_REG_DRV_FREQ_INFO,           /**/ \
      MI80_115_REG_DRV_DUTY_CYCLE_INFO, /**/ \
      MI80_115_REG_ADC_ON_VAL_INFO,     /**/ \
      MI80_115_REG_ADC_OFF_VAL_INFO,    /**/ \
      MI80_115_REG_ADC_SHUNT_VAL_INFO,  /**/ \
      MI80_115_REG_DRV_ON_MV_INFO,      /**/ \
      MI80_115_REG_DRV_OFF_MV_INFO,     /**/ \
      MI80_115_REG_DRV_SHUNT_MV_INFO,   /**/ \
      MI80_115_REG_DRV_ENABLE_INFO,     /**/ \
      MI80_115_REG_IO_1_MODE_INFO,      /**/ \
      MI80_115_REG_IO_2_MODE_INFO,      /**/ \
      MI80_115_REG_IO_3_MODE_INFO,      /**/ \
      MI80_115_REG_IO_4_MODE_INFO,      /**/ \
      MI80_115_REG_IO_1_INFO,           /**/ \
      MI80_115_REG_IO_2_INFO,           /**/ \
      MI80_115_REG_IO_3_INFO,           /**/ \
      MI80_115_REG_IO_4_INFO,           /**/ \
      MI80_REG_I2C_STATUS_INFO,         /**/ \
      MI80_REG_I2C_ADRRESS_INFO,        /**/ \
      MI80_REG_MODULE_ID_INFO,          /**/ \
      MI80_REG_MODULE_INFO_INFO,        /**/ \
      MI80_REG_MODULE_DESCRIPTION_INFO, /**/ \
      MI80_REG_INT_ENABLE_INFO,         /**/ \
      MI80_REG_INT_FLAGS_INFO,          /**/ \
      MI80_CMD_RESET_INFO,              /**/ \
      MI80_CMD_PERSIST_CONFIG_INFO,     /**/ \
      MI80_IO_SINT_GPIO_INFO,           /**/ \
      {0},                              /**/

#define MI80_115_INTERRUPT_INFO                                                          \
  {                                                                                      \
    {MI80_115_IO1_INT_RISE_FLAG, "io1Rise", "IO 1 Rise", "IO #1 rising edge"},      /**/ \
        {MI80_115_IO1_INT_FALL_FLAG, "io1Fall", "IO 1 Fall", "IO #1 falling edge"}, /**/ \
        {MI80_115_IO2_INT_RISE_FLAG, "io2Rise", "IO 2 Rise", "IO #2 rising edge"},  /**/ \
        {MI80_115_IO2_INT_FALL_FLAG, "io2Fall", "IO 2 Fall", "IO #2 falling edge"}, /**/ \
        {MI80_115_IO3_INT_RISE_FLAG, "io3Rise", "IO 3 Rise", "IO #3 rising edge"},  /**/ \
        {MI80_115_IO3_INT_FALL_FLAG, "io3Fall", "IO 3 Fall", "IO #3 falling edge"}, /**/ \
        {MI80_115_IO4_INT_RISE_FLAG, "io4Rise", "IO 4 Rise", "IO #4 rising edge"},  /**/ \
        {MI80_115_IO4_INT_FALL_FLAG, "io4Fall", "IO 4 Fall", "IO #4 falling edge"}, /**/ \
        {0},                                                                        /**/ \
  }

#define MI80_115_I2C_MODULE_INFO \
  { MI80_115_DEFAULT_I2C_ADDRESS, 103, "Mi80-103", "Mi80-103", "Mi80-103 Mosfet Driver", MI80_115_REGS_I2C_INFO, MI80_115_INTERRUPT_INFO }

#endif
