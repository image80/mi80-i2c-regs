#ifndef _MI80_102_REGS_H_
#define _MI80_102_REGS_H_

#include <mi80_common_regs.h>

#define MI80_102_DEFAULT_I2C_ADDRESS 0x42
#define MI80_102_MODULE_ID 102
#define MI80_102_MODULE_NAME "Mi80-102"
#define MI80_102_MODULE_FEATURES HAS_STATUS_LED
#define MI80_102_MODULE_DESCRIPTION "Mi80-102 Stepper Driver"

typedef enum Mi80_102_InterrruptFlags_enum {
  MI80_102_STEPPER_READY_INT_FLAG = 0x01,
  MI80_102_STEPPER_FAULT_INT_FLAG = 0x02,

  MI80_102_LIMIT1_INT_RISE_FLAG = 0x100,
  MI80_102_LIMIT1_INT_FALL_FLAG = 0x200,
  MI80_102_LIMIT2_INT_RISE_FLAG = 0x400,
  MI80_102_LIMIT2_INT_FALL_FLAG = 0x800,

} Mi80_102_InterruptFlags;

typedef enum Mi80_102_Register_enum {
  MI80_102_REG_STATUS = 0x00,
  MI80_102_REG_DEFAULT_DIR = 0x01,
  MI80_102_REG_STEP_MODE = 0x02,
  MI80_102_REG_MAX_STEP_INTERVAL = 0x03,
  MI80_102_REG_ACCEL_RATE = 0x04,
  MI80_102_REG_MAX_STEP_FREQ = 0x05,
  MI80_102_REG_REMAINING_STEPS = 0x06,

  MI80_102_CMD_STEPPER_MOVE = 0x20,
  MI80_102_CMD_STEPPER_RESET = 0x21,
  MI80_102_CMD_STEPPER_ENABLE = 0x22,

  MI80_102_REG_IO_1_MODE = MI80_REG_IO_MODE_BASE,
  MI80_102_REG_IO_1 = MI80_REG_IO_BASE,
  MI80_102_REG_IO_2_MODE = MI80_REG_IO_MODE_BASE + 1,
  MI80_102_REG_IO_2 = MI80_REG_IO_BASE + 1,

} Mi80_102_Register;

typedef enum Mi80_102_StepperMode_enum {
  MI80_102_STEPPER_MODE_FULL = 0,
  MI80_102_STEPPER_MODE_HALF = 1,
  MI80_102_STEPPER_MODE_QUARTER = 2,
  MI80_102_STEPPER_MODE_EIGHTH = 3,
  MI80_102_STEPPER_MODE_SIXTEENTH = 4,
  MI80_102_STEPPER_MODE_THIRTYSECOND = 5

} Mi80_102_StepperMode;

#define MI80_102_REG_STATUS_DEF \
  { MI80_102_REG_STATUS, I2C_DATA_TYPE_UINT8, 1, MI80_REG_RO }
#define MI80_102_REG_DEFAULT_DIR_DEF \
  { MI80_102_REG_DEFAULT_DIR, I2C_DATA_TYPE_UINT8, 1, MI80_REG_RW }
#define MI80_102_REG_STEP_MODE_DEF \
  { MI80_102_REG_STEP_MODE, I2C_DATA_TYPE_UINT8, 1, MI80_REG_RW }
#define MI80_102_REG_MAX_STEP_INTERVAL_DEF \
  { MI80_102_REG_MAX_STEP_INTERVAL, I2C_DATA_TYPE_UINT16, 1, MI80_REG_RW }
#define MI80_102_REG_ACCEL_RATE_DEF \
  { MI80_102_REG_ACCEL_RATE, I2C_DATA_TYPE_UINT8, 1, MI80_REG_RW }
#define MI80_102_REG_MAX_STEP_FREQ_DEF \
  { MI80_102_REG_MAX_STEP_FREQ, I2C_DATA_TYPE_UINT16, 1, MI80_REG_RW }
#define MI80_102_REG_REMAINING_STEPS_DEF \
  { MI80_102_REG_REMAINING_STEPS, I2C_DATA_TYPE_UINT32, 1, MI80_REG_RO }
#define MI80_102_CMD_STEPPER_MOVE_DEF \
  { MI80_102_CMD_STEPPER_MOVE, I2C_DATA_TYPE_INT32, 1, MI80_REG_RW }
#define MI80_102_CMD_STEPPER_RESET_DEF \
  { MI80_102_CMD_STEPPER_RESET, I2C_DATA_TYPE_UINT8, 1, MI80_REG_RW }
#define MI80_102_CMD_STEPPER_ENABLE_DEF \
  { MI80_102_CMD_STEPPER_ENABLE, I2C_DATA_TYPE_UINT8, 1, MI80_REG_RW }

#define MI80_102_REG_IO_1_MODE_DEF \
  { MI80_102_REG_IO_1_MODE, I2C_DATA_TYPE_UINT8, 6, MI80_REG_RW }
#define MI80_102_REG_IO_2_MODE_DEF \
  { MI80_102_REG_IO_2_MODE, I2C_DATA_TYPE_UINT8, 6, MI80_REG_RW }
#define MI80_102_REG_IO_1_DEF \
  { MI80_102_REG_IO_1, I2C_DATA_TYPE_UINT8, 1, MI80_REG_RW }
#define MI80_102_REG_IO_2_DEF \
  { MI80_102_REG_IO_2, I2C_DATA_TYPE_UINT8, 1, MI80_REG_RW }

#define MI80_102_REG_STATUS_INFO \
  { "status", "Status", "Indicates if stepper is busy", MI80_102_REG_STATUS_DEF }
#define MI80_102_REG_DEFAULT_DIR_INFO \
  { "defaultDir", "Default Dir", "Set or get default direction", MI80_102_REG_DEFAULT_DIR_DEF }
#define MI80_102_REG_STEP_MODE_INFO \
  { "stepMode", "Step Mode", "Set or get microstep mode", MI80_102_REG_STEP_MODE_DEF }
#define MI80_102_REG_MAX_STEP_INTERVAL_INFO \
  { "maxStepInterval", "Max Step Interval", "Set or get max step interval", MI80_102_REG_MAX_STEP_INTERVAL_DEF }
#define MI80_102_REG_ACCEL_RATE_INFO \
  { "accelRate", "Accel Rate", "Set or get accelleration increment", MI80_102_REG_ACCEL_RATE_DEF }
#define MI80_102_REG_MAX_STEP_FREQ_INFO \
  { "maxFreq", "Max Step Freq", "Set or get max step frequency", MI80_102_REG_MAX_STEP_FREQ_DEF }
#define MI80_102_REG_REMAINING_STEPS_INFO \
  { "remSteps", "Rem Steps", "Get remaining steps", MI80_102_REG_REMAINING_STEPS_DEF }
#define MI80_102_CMD_STEPPER_MOVE_INFO \
  { "cmdMove", "Move Command", "Move stepper +/- steps", MI80_102_CMD_STEPPER_MOVE_DEF }
#define MI80_102_CMD_STEPPER_RESET_INFO \
  { "cmdStepReset", "Reset Stepper", "Reset stepper motor", MI80_102_CMD_STEPPER_RESET_DEF }
#define MI80_102_CMD_STEPPER_ENABLE_INFO \
  { "enable", "Enable Stepper", "Enable or disable stepper (sleep/wakeup)", MI80_102_CMD_STEPPER_ENABLE_DEF }

#define MI80_102_REG_IO_1_MODE_INFO \
  { "ioMode1", "IO 1 Mode", "Get or set mode of IO #1 (Limit 1)", MI80_102_REG_IO_1_MODE_DEF }
#define MI80_102_REG_IO_2_MODE_INFO \
  { "ioMode2", "IO 2 Mode", "Get or set mode of IO #2 (Limit 2)", MI80_102_REG_IO_2_MODE_DEF }
#define MI80_102_REG_IO_1_INFO \
  { "io1", "IO 1", "Get or set value of IO #1 (Limit 1)", MI80_102_REG_IO_1_DEF }
#define MI80_102_REG_IO_2_INFO \
  { "io2", "IO 2", "Get or set value of IO #2 (Limit 2)", MI80_102_REG_IO_2_DEF }

#define MI80_102_REG_MODULE_DESCRIPTION_DEF \
  { MI80_REG_MODULE_DESCRIPTION, I2C_DATA_TYPE_STRING, sizeof(MI80_102_MODULE_DESCRIPTION), MI80_REG_RO }
#define MI80_REG_102_MODULE_DESCRIPTION_INFO \
  { "description", "Description", "Module description", MI80_102_REG_MODULE_DESCRIPTION_DEF }

#define MI80_102_I2C_REG_INFO                      \
  {                                                \
    MI80_102_REG_STATUS_INFO,                 /**/ \
        MI80_102_REG_DEFAULT_DIR_INFO,        /**/ \
        MI80_102_REG_STEP_MODE_INFO,          /**/ \
        MI80_102_REG_MAX_STEP_INTERVAL_INFO,  /**/ \
        MI80_102_REG_ACCEL_RATE_INFO,         /**/ \
        MI80_102_REG_MAX_STEP_FREQ_INFO,      /**/ \
        MI80_102_REG_REMAINING_STEPS_INFO,    /**/ \
        MI80_102_CMD_STEPPER_MOVE_INFO,       /**/ \
        MI80_102_CMD_STEPPER_RESET_INFO,      /**/ \
        MI80_102_CMD_STEPPER_ENABLE_INFO,     /**/ \
        MI80_REG_I2C_ADRRESS_INFO,            /**/ \
        MI80_REG_MODULE_ID_INFO,              /**/ \
        MI80_REG_MODULE_INFO_INFO,            /**/ \
        MI80_REG_102_MODULE_DESCRIPTION_INFO, /**/ \
        MI80_REG_INT_ENABLE_INFO,             /**/ \
        MI80_REG_INT_FLAGS_INFO,              /**/ \
        MI80_CMD_RESET_INFO,                  /**/ \
        MI80_CMD_PERSIST_CONFIG_INFO,         /**/ \
        MI80_CMD_PERSIST_ERASE_INFO,          /**/ \
        MI80_REG_SINT_GPIO_INFO,              /**/ \
        MI80_102_REG_IO_1_MODE_INFO,          /**/ \
        MI80_102_REG_IO_2_MODE_INFO,          /**/ \
        MI80_102_REG_IO_1_INFO,               /**/ \
        MI80_102_REG_IO_2_INFO,               /**/ \
        {NULL},                               /**/ \
  }

#define MI80_102_INTERRUPT_INFO                                                                              \
  {                                                                                                          \
    {MI80_102_STEPPER_READY_INT_FLAG, "stepperReady", "Stepper Ready", "Steps complete/stepper ready"}, /**/ \
        {MI80_102_STEPPER_FAULT_INT_FLAG, "stepperFault", "Stepper Fault", "Stepper fault condition"},  /**/ \
        {MI80_102_LIMIT1_INT_RISE_FLAG, "limit1Rise", "Limit 1 Rise", "Limit 1/IO #1 rising edge"},     /**/ \
        {MI80_102_LIMIT1_INT_FALL_FLAG, "limit1Fall", "Limit 1 Fall", "Limit 1/IO #1 falling edge"},    /**/ \
        {MI80_102_LIMIT2_INT_RISE_FLAG, "limit2Rise", "Limit 2 Rise", "Limit 2/IO #2 rising edge"},     /**/ \
        {MI80_102_LIMIT2_INT_FALL_FLAG, "limi2Fall", "Limit 2 Fall", "Limit 2/IO #2 falling edge"},     /**/ \
        {0},                                                                                            /**/ \
  }

#define MI80_102_I2C_MODULE_INFO \
  { MI80_102_DEFAULT_I2C_ADDRESS, 102, "Mi80-102", "Mi80-102", "Mi80-102 Stepper motor driver", MI80_102_I2C_REG_INFO, MI80_102_INTERRUPT_INFO }

#endif
