#pragma once

#include <stdint.h>

#ifdef MI80_STM8
#include <stm8s.h>
#endif

#ifndef bool
// #include <stdbool.h>
#endif

#define MAX_I2C_DATA_LENGTH 64
#define MAX_MI80_DESCRIPTION_SIZE 32
#define MAX_MI80_NAME_SIZE 16
#define MAX_I2C_REGISTERS 64
#define MAX_I2C_INT_FLAGS 32

#define MI80_I2C_INT_VALID_FLAG 0x80000000UL

#define MIN_I2C_ADDRESS 0x08
#define MAX_I2C_ADDRESS 0x7e

#define IS_I2C_ADDRESS_VALID(addr) (addr >= MIN_I2C_ADDRESS && addr <= MAX_I2C_ADDRESS)
#define IS_I2C_ADDRESS_INVALID(addr) !IS_I2C_ADDRESS_VALID(addr)

#define I2C_3RD_PARTY_MODULE_BASE_ID 1000
#define IS_MODULE_GENERIC_I2C(moduleId) (moduleId < 100)
#define IS_MODULE_MI80(moduleId) (moduleId >= 100 && moduleId < I2C_3RD_PARTY_MODULE_BASE_ID)
#define IS_MODULE_3RD_PARTY_I2C(moduleId) (moduleId >= I2C_3RD_PARTY_MODULE_BASE_ID)

typedef uint8_t registerId_t;

#define MI80_IO_MODE_DISABLED 0x00
#define MI80_IO_MODE_INPUT 0x00
#define MI80_IO_MODE_HIZ 0x02
#define MI80_IO_MODE_FLOAT 0x00
#define MI80_IO_MODE_PULLUP 0x02
#define MI80_IO_MODE_INPUT_INT_RISE 0x04
#define MI80_IO_MODE_INPUT_PU_INT_RISE (MI80_IO_MODE_INPUT_INT_RISE | MI80_IO_MODE_PULLUP)
#define MI80_IO_MODE_INPUT_INT_FALL 0x08
#define MI80_IO_MODE_INPUT_PU_INT_FALL (MI80_IO_MODE_INPUT_INT_FALL | MI80_IO_MODE_PULLUP)
#define MI80_IO_MODE_INPUT_INT_RISE_FALL (MI80_IO_MODE_INPUT_INT_FALL | MI80_IO_MODE_INPUT_INT_RISE)
#define MI80_IO_MODE_INPUT_INT_PU_RISE_FALL (MI80_IO_MODE_INPUT_INT_RISE_FALL | MI80_IO_MODE_PULLUP)
#define MI80_IO_MODE_INPUT_ANALOG 0x10
#define MI80_IO_MODE_OUTPUT 0x01
#define MI80_IO_MODE_OUTPUT_HIZ (MI80_IO_MODE_OUTPUT | MI80_IO_MODE_HIZ)
#define MI80_IO_MODE_PWM (0x04 | MI80_IO_MODE_OUTPUT)
#define MI80_IO_MODE_PWM_HIZ (MI80_IO_MODE_PWM | MI80_IO_MODE_OUTPUT_HIZ)
#define MI80_IO_MODE_SERVO (0x08 | MI80_IO_MODE_OUTPUT)
#define MI80_IO_MODE_LED_STRIP (0x10 | MI80_IO_MODE_OUTPUT)
#define MI80_IO_MODE_TXD (0x20 | MI80_IO_MODE_OUTPUT)
#define MI80_IO_MODE_RXD (0x20 | MI80_IO_MODE_INPUT)
#define MI80_IO_MODE_RXD_PU (MI80_IO_MODE_RXD | MI80_IO_MODE_PULLUP)
#define MI80_IO_MODE_BUTTON (0x40 | MI80_IO_MODE_INPUT_INT_PU_RISE_FALL)

#define MI80_IO_MODE_ALL 0xffff
#define MI80_IO_MODE_ALL_NO_ANALOG 0xffef

#define MI80_IO_MODE_IS_INPUT(mode) ((mode & MI80_IO_MODE_OUTPUT) == 0)
#define MI80_IO_MODE_IS_INPUT_PULLUP(mode) (MI80_IO_MODE_IS_INPUT(mode) && (mode & MI80_IO_MODE_PULLUP == MI80_IO_MODE_PULLUP))
#define MI80_IO_MODE_IS_INPUT_FLOAT(mode) (MI80_IO_MODE_IS_INPUT(mode) && !MI80_IO_MODE_IS_INPUT_PULLUP(mode))

#define MI80_IO_MODE_IS_INTERRUPT(mode) \
  (MI80_IO_MODE_IS_INPUT(mode) && (mode & (MI80_IO_MODE_INPUT_INT_RISE | MI80_IO_MODE_INPUT_INT_FALL)))

#define MI80_IO_MODE_IS_OUTPUT(mode) (mode & MI80_IO_MODE_OUTPUT)
#define MI80_IO_MODE_IS_OUTPUT_HIZ(mode) (MI80_IO_MODE_IS_OUTPUT(mode) && (mode & MI80_IO_MODE_HIZ))
#define MI80_IO_MODE_IS_PWM(mode) ((mode & MI80_IO_MODE_PWM) == MI80_IO_MODE_PWM)
#define MI80_IO_MODE_IS_SERVO(mode) ((mode & MI80_IO_MODE_SERVO) == MI80_IO_MODE_SERVO)
#define MI80_IO_MODE_IS_PWM_OR_SERVO(mode) (MI80_IO_MODE_IS_PWM(mode) || MI80_IO_MODE_IS_SERVO(mode))
#define MI80_IO_MODE_IS_LED_STRIP(mode) ((mode & MI80_IO_MODE_LED_STRIP) == MI80_IO_MODE_LED_STRIP)
#define MI80_IO_MODE_IS_ANALOG_INPUT(mode) (MI80_IO_MODE_IS_INPUT(mode) && (mode & MI80_IO_MODE_INPUT_ANALOG))
#define MI80_IO_MODE_IS_DIGITAL_INPUT(mode) (!MI80_IO_MODE_IS_ANALOG_INPUT(mode) && MI80_IO_MODE_IS_INPUT(mode))
#define MI80_IO_MODE_IS_TXD(mode) ((mode & MI80_IO_MODE_TXD) == MI80_IO_MODE_TXD)
#define MI80_IO_MODE_IS_RXD(mode) (MI80_IO_MODE_IS_INPUT(mode) && ((mode & MI80_IO_MODE_RXD) == MI80_IO_MODE_RXD))
#define MI80_IO_MODE_IS_BUTTON(mode) ((mode & MI80_IO_MODE_BUTTON) == MI80_IO_MODE_BUTTON)

typedef enum Mi80_RGB_Color_enum {
  RED = 0xff0000,
  GREEN = 0x00ff00,
  BLUE = 0x0000ff,
  CYAN = 0x00ffff,
  MAGENTA = 0xff00ff,
  YELLOW = 0xffff00
} Mi80_RGB_Color;

typedef enum Mi80_ButtonMode_enum {
  MI80_BUTTON_MODE_DISABLED = 0x00,
  MI80_BUTTON_MODE_ON_PRESS,
  MI80_BUTTON_MODE_ON_RELEASE,
  MI80_BUTTON_MODE_ON_PRESS_AND_RELEASE,
  MI80_BUTTON_MODE_MULTIPRESS,

  MI80_BUTTON_MODE_INVALID = 0xff
} Mi80_ButtonMode;

#define MI80_BUTTON_INT_FLAG(buttonIx) (0x01 << (buttonIx & 0xff)
#define MI80_IO_INT_FLAG(ioIx) (0x100 << (ioIx & 0xff)

typedef struct Mi80_ButtonState_struct {
  bool enabled;
  bool pressed;
  uint8_t timesPressed;
  bool valid;
} Mi80_ButtonState;

typedef enum Mi80_FeatureFlag_enum { HAS_STATUS_LED = 1 } Mi80_FeatureFlag;

#define MI80_MODULE_INFO_VERSION 1

typedef struct Mi80_ModuleInfo_struct {
  uint8_t moduleInfoVersion;
  char name[MAX_MI80_NAME_SIZE];
  uint16_t moduleId;
  uint16_t moduleVersion;
  uint16_t firmwareVersion;
  uint8_t numberOfRgbLeds;
  uint8_t numberOfButtons;
  uint8_t numberOfIO;
  uint16_t features;
  uint16_t i2cBufferSize;
} Mi80_ModuleInfo;

typedef enum Mi80I2cDataType_enum {
  I2C_DATA_TYPE_BOOL = 0,
  I2C_DATA_TYPE_UINT8 = 1,
  I2C_DATA_TYPE_UINT16 = 2,
  I2C_DATA_TYPE_UINT32 = 3,
  I2C_DATA_TYPE_INT8 = 4,
  I2C_DATA_TYPE_INT16 = 5,
  I2C_DATA_TYPE_INT32 = 6,
  I2C_DATA_TYPE_STRING = 7,
  I2C_DATA_TYPE_RGB = 8,
  I2C_DATA_TYPE_HSV = 9,
  I2C_DATA_TYPE_GRB = 10,  // same as RGB but order GRB for direct led writes (e.g. ledstrip)
  I2C_DATA_TYPE_INVALID = -1
} Mi80I2cDataType;

typedef struct Mi80RGBData_struct {
  uint8_t red;
  uint8_t green;
  uint8_t blue;
} Mi80RgbData;

typedef struct Mi80HsvColor_struct {
  uint8_t hue;
  uint8_t saturation;
  uint8_t value;
} Mi80HsvColor;

typedef struct Mi80GrbData_struct {
  uint8_t green;
  uint8_t red;
  uint8_t blue;
} Mi80GrbData;

typedef enum Mi80I2cRegisterAccess_enum { MI80_REG_RO = 0, MI80_REG_WO = 1, MI80_REG_RW = 2 } Mi80I2cRegisterAccess;

typedef struct Mi80I2cRegister_struct {
  registerId_t registerId;
  Mi80I2cDataType dataType;
  uint8_t size;
  Mi80I2cRegisterAccess access;
} Mi80I2cRegister;

typedef struct Mi80I2cRegisterInfo_struct {
  const char *name;
  const char *label;
  const char *description;
  Mi80I2cRegister i2cRegister;
} Mi80I2cRegisterInfo;

typedef struct Mi80I2cInterruptInfo_struct {
  uint32_t flag;
  const char *name;
  const char *label;
  const char *description;
} Mi80I2cInterruptInfo;

typedef struct Mi80I2cModuleInfo_struct {
  int defaultAddress;
  int moduleId;
  const char *name;
  const char *label;
  const char *description;
  Mi80I2cRegisterInfo i2cRegisterInfo[MAX_I2C_REGISTERS];
  Mi80I2cInterruptInfo interruptInfo[MAX_I2C_INT_FLAGS];
} Mi80I2cModuleInfo;

#define MI80_I2C_REGISTER_LIST_SIZE(regList) (sizeof(regList / sizeof(Mi80I2cRegister)))

typedef enum Mi80_Common_Register_enum {
  MI80_REG_I2C_STATUS = 0x80,
  MI80_REG_I2C_ADDRESS = 0x81,

  MI80_REG_MODULE_ID = 0x82,
  MI80_REG_MODULE_INFO = 0x83,
  MI80_REG_MODULE_DESCRIPTION = 0x84,

  MI80_REG_INT_ENABLE = 0x85,
  MI80_REG_INT_FLAGS = 0x86,

  MI80_CMD_RESET = 0x87,
  MI80_CMD_PERSIST_CONFIG = 0x88,
  MI80_CMD_PERSIST_ERASE = 0x8F,

  MI80_REG_STATUS = 0x89,

  MI80_REG_STATLED = 0x8A,

  MI80_REG_PWM1_FREQ = 0x8B,
  MI80_REG_PWM2_FREQ = 0x8C,

  MI80_IO_DEBOUNCE_MS = 0x8D,

  MI80_REG_SINT_GPIO = 0x8E,

  MI80_REG_RGB_LED_BASE = 0xB0,

  MI80_REG_BUTTON_MODE_BASE = 0xC0,
  MI80_REG_BUTTON_BASE = 0xC8,

  MI80_REG_IO_MODE_BASE = 0xD0,
  MI80_REG_IO_BASE = 0xE0,

  MI80_REG_INVALID = 0xFF  // reserved to indicate invalid reg id

} Mi80_Common_Register;

#define MI80_REG_I2C_STATUS_DEF \
  { MI80_REG_I2C_STATUS, I2C_DATA_TYPE_UINT8, 32, MI80_REG_RO }
#define MI80_REG_I2C_ADDRESS_DEF \
  { MI80_REG_I2C_ADDRESS, I2C_DATA_TYPE_UINT8, 1, MI80_REG_RW }
#define MI80_REG_MODULE_ID_DEF \
  { MI80_REG_MODULE_ID, I2C_DATA_TYPE_UINT16, 1, MI80_REG_RO }
#define MI80_REG_MODULE_INFO_DEF \
  { MI80_REG_MODULE_INFO, I2C_DATA_TYPE_UINT8, sizeof(Mi80_ModuleInfo), MI80_REG_RO }
#define MI80_REG_MODULE_DESCRIPTION_DEF \
  { MI80_REG_MODULE_DESCRIPTION, I2C_DATA_TYPE_STRING, MAX_MI80_DESCRIPTION_SIZE, MI80_REG_RO }
#define MI80_REG_INT_ENABLE_DEF \
  { MI80_REG_INT_ENABLE, I2C_DATA_TYPE_UINT32, 1, MI80_REG_RW }
#define MI80_REG_INT_FLAGS_DEF \
  { MI80_REG_INT_FLAGS, I2C_DATA_TYPE_UINT32, 1, MI80_REG_RO }
#define MI80_CMD_RESET_DEF \
  { MI80_CMD_RESET, I2C_DATA_TYPE_UINT32, 1, MI80_REG_WO }
#define MI80_CMD_PERSIST_CONFIG_DEF \
  { MI80_CMD_PERSIST_CONFIG, I2C_DATA_TYPE_UINT16, 1, MI80_REG_WO }
#define MI80_CMD_PERSIST_ERASE_DEF \
  { MI80_CMD_PERSIST_ERASE, I2C_DATA_TYPE_UINT8, 1, MI80_REG_WO }
#define MI80_REG_STATUS_DEF \
  { MI80_REG_STATUS, I2C_DATA_TYPE_UINT32, 1, MI80_REG_RO }
#define MI80_REG_STATLED_DEF \
  { MI80_REG_STATLED, I2C_DATA_TYPE_UINT16, 1, MI80_REG_RW }
#define MI80_REG_SINT_GPIO_DEF \
  { MI80_REG_SINT_GPIO, I2C_DATA_TYPE_UINT8, 1, MI80_REG_RW }
#define MI80_REG_PWM1_FREQ_DEF \
  { MI80_REG_PWM1_FREQ, I2C_DATA_TYPE_UINT16, 1, MI80_REG_RW }
#define MI80_REG_PWM2_FREQ_DEF \
  { MI80_REG_PWM2_FREQ, I2C_DATA_TYPE_UINT16, 1, MI80_REG_RW }

#define MI80_DEFAULT_COMMON_REGS_DEF        \
  MI80_REG_I2C_STATUS_DEF,             /**/ \
      MI80_REG_I2C_ADDRESS_DEF,        /**/ \
      MI80_REG_MODULE_ID_DEF,          /**/ \
      MI80_REG_MODULE_INFO_DEF,        /**/ \
      MI80_REG_MODULE_DESCRIPTION_DEF, /**/ \
      MI80_REG_INT_ENABLE_DEF,         /**/ \
      MI80_REG_INT_FLAGS_DEF,          /**/ \
      MI80_CMD_RESET_DEF,              /**/ \
      MI80_CMD_PERSIST_CONFIG_DEF,     /**/ \
      MI80_CMD_PERSIST_ERASE_DEF,      /**/ \
      MI80_REG_STATUS_DEF,             /**/ \
      MI80_REG_STATLED_DEF,            /**/ \
      MI80_REG_SINT_GPIO_DEF,          /**/ \
      MI80_REG_PWM1_FREQ_DEF,          /**/ \
      MI80_REG_PWM2_FREQ_DEF,          /**/

#define MI80_REG_I2C_STATUS_INFO \
  { "i2cStatus", "I2C Status", "I2C status dword", MI80_REG_I2C_STATUS_DEF }
#define MI80_REG_I2C_ADRRESS_INFO \
  { "i2cAddress", "I2C Address", "Get or set the I2C address", MI80_REG_I2C_ADDRESS_DEF }
#define MI80_REG_MODULE_ID_INFO \
  { "moduleId", "Module ID", "Mi80 module ID", MI80_REG_MODULE_ID_DEF }
#define MI80_REG_MODULE_INFO_INFO \
  { "moduleInfo", "Module Info", "Mi80 Module Info", MI80_REG_MODULE_INFO_DEF }
#define MI80_REG_MODULE_DESCRIPTION_INFO \
  { "description", "Description", "Module description", MI80_REG_MODULE_DESCRIPTION_DEF }
#define MI80_REG_INT_ENABLE_INFO \
  { "intEnable", "Interrupt Enable Flags", "Interrupt enable flags", MI80_REG_INT_ENABLE_DEF }
#define MI80_REG_INT_FLAGS_INFO \
  { "intFlags", "Interrupt Flags", "Interrupt status flags (read will clear interrupts)", MI80_REG_INT_FLAGS_DEF }
#define MI80_CMD_RESET_INFO \
  { "cmdReset", "Reset Command", "Reset the device with optional flags", MI80_CMD_RESET_DEF }
#define MI80_CMD_PERSIST_CONFIG_INFO \
  { "cmdPersist", "Persist Command", "Persist current device config to NVM (optionalflags)", MI80_CMD_PERSIST_CONFIG_DEF }
#define MI80_CMD_PERSIST_ERASE_INFO \
  { "cmdNvmErase", "NVM Erase Command", "Erase all persistent NVM data", MI80_CMD_PERSIST_ERASE_DEF }
#define MI80_REG_STATUS_INFO \
  { "status", "General Status", "Read the device status", MI80_REG_STATUS_DEF }
#define MI80_REG_STATLED_INFO \
  { "statusLed", "Status LED", "Read or write the Status LED state", MI80_REG_STATLED_DEF }
#define MI80_REG_SINT_GPIO_INFO \
  { "sintGpio", "SINT GPIO Pin", "Get or set the current SINT GPIO pin", MI80_REG_SINT_GPIO_DEF }
#define MI80_REG_PWM1_FREQ_INFO \
  { "pwm1Freq", "PWM 1 Freq", "Get or set the PWM (TIM) 1 frequency", MI80_REG_PWM1_FREQ_DEF }
#define MI80_REG_PWM2_FREQ_INFO \
  { "pwm2Freq", "PWM 2 Freq", "Get or set the PWM (TIM) 2 frequency", MI80_REG_PWM2_FREQ_DEF }

#define MI80_DEFAULT_COMMON_REGS_INFO        \
  MI80_REG_I2C_STATUS_INFO,             /**/ \
      MI80_REG_I2C_ADRRESS_INFO,        /**/ \
      MI80_REG_MODULE_ID_INFO,          /**/ \
      MI80_REG_MODULE_INFO_INFO,        /**/ \
      MI80_REG_MODULE_DESCRIPTION_INFO, /**/ \
      MI80_REG_INT_ENABLE_INFO,         /**/ \
      MI80_REG_INT_FLAGS_INFO,          /**/ \
      MI80_CMD_RESET_INFO,              /**/ \
      MI80_CMD_PERSIST_CONFIG_INFO,     /**/ \
      MI80_CMD_PERSIST_ERASE_INFO,      /**/ \
      MI80_REG_STATUS_INFO,             /**/ \
      MI80_REG_STATLED_INFO,            /**/ \
      MI80_REG_SINT_GPIO_INFO,          /**/ \
      MI80_REG_PWM1_FREQ_INFO,          /**/ \
      MI80_REG_PWM2_FREQ_INFO,          /**/

///////////////////////////////////////////////////////////////////////////////////
// XIO

#define MI80_REG_IO_1_MODE MI80_REG_IO_MODE_BASE
#define MI80_REG_IO_2_MODE MI80_REG_IO_MODE_BASE + 1
#define MI80_REG_IO_3_MODE MI80_REG_IO_MODE_BASE + 2
#define MI80_REG_IO_4_MODE MI80_REG_IO_MODE_BASE + 3

#define MI80_REG_IO_1_MODE_DEF \
  { MI80_REG_IO_1_MODE, I2C_DATA_TYPE_UINT16, 4, MI80_REG_RW }
#define MI80_REG_IO_2_MODE_DEF \
  { MI80_REG_IO_2_MODE, I2C_DATA_TYPE_UINT16, 4, MI80_REG_RW }
#define MI80_REG_IO_3_MODE_DEF \
  { MI80_REG_IO_3_MODE, I2C_DATA_TYPE_UINT16, 4, MI80_REG_RW }
#define MI80_REG_IO_4_MODE_DEF \
  { MI80_REG_IO_4_MODE, I2C_DATA_TYPE_UINT16, 4, MI80_REG_RW }

#define MI80_REG_IO_1_MODE_INFO \
  { "ioMode1", "IO 1 Mode", "Get or set mode of IO #1", MI80_REG_IO_1_MODE_DEF }
#define MI80_REG_IO_2_MODE_INFO \
  { "ioMode2", "IO 2 Mode", "Get or set mode of IO #2", MI80_REG_IO_2_MODE_DEF }
#define MI80_REG_IO_3_MODE_INFO \
  { "ioMode3", "IO 3 Mode", "Get or set mode of IO #3", MI80_REG_IO_3_MODE_DEF }
#define MI80_REG_IO_4_MODE_INFO \
  { "ioMode4", "IO 4 Mode", "Get or set mode of IO #4", MI80_REG_IO_4_MODE_DEF }

#define MI80_REG_IO_1 MI80_REG_IO_BASE
#define MI80_REG_IO_2 MI80_REG_IO_BASE + 1
#define MI80_REG_IO_3 MI80_REG_IO_BASE + 2
#define MI80_REG_IO_4 MI80_REG_IO_BASE + 3

#define MI80_REG_IO_1_DEF \
  { MI80_REG_IO_1, I2C_DATA_TYPE_UINT16, 1, MI80_REG_RW }
#define MI80_REG_IO_2_DEF \
  { MI80_REG_IO_2, I2C_DATA_TYPE_UINT16, 1, MI80_REG_RW }
#define MI80_REG_IO_3_DEF \
  { MI80_REG_IO_3, I2C_DATA_TYPE_UINT16, 1, MI80_REG_RW }
#define MI80_REG_IO_4_DEF \
  { MI80_REG_IO_4, I2C_DATA_TYPE_UINT16, 1, MI80_REG_RW }

#define MI80_REG_IO_1_INFO \
  { "io1", "IO 1", "Get or set value of IO #1", MI80_REG_IO_1_DEF }
#define MI80_REG_IO_2_INFO \
  { "io2", "IO 2", "Get or set value of IO #2", MI80_REG_IO_2_DEF }
#define MI80_REG_IO_3_INFO \
  { "io3", "IO 3", "Get or set value of IO #3", MI80_REG_IO_3_DEF }
#define MI80_REG_IO_4_INFO \
  { "io4", "IO 4", "Get or set value of IO #4", MI80_REG_IO_4_DEF }

///////////////////////////////////////////////////////////////////////////////////////////
