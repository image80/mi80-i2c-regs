#ifndef _MI80_106_REGS_H_
#define _MI80_106_REGS_H_

#include "mi80_common_regs.h"

#define MI80_106_DEFAULT_I2C_ADDRESS 0x46
#define MI80_106_MODULE_ID 106
#define MI80_106_MODULE_NAME "Mi80-106"
#define MI80_106_MODULE_FEATURES HAS_STATUS_LED
#define MI80_106_MODULE_DESCRIPTION "Mi80-106 Alcohol sensor"

#define MI80_106_NMBR_RGB_LEDS 3
#define MI80_106_NMBR_BUTTONS 2
#define MI80_106_NMBR_IO 2

#define MI80_XIO_INT_FLAG_BASE 0x40008000UL

typedef struct Mi80_106_SensorConfig_struct {
  uint16_t warmupTimeSec;
  uint8_t sampleTimeSec;
  uint8_t recoverTimeSec;
  uint16_t lowThreshold;
  uint16_t warnThreshold;
  uint16_t alarmThreshold;
  uint16_t highThreshold;
  uint16_t autoTurnOffSec;
} Mi80_106_SensorConfig;

typedef struct Mi80_106_SensorStatusInfo_struct {
  uint8_t enabled;
  uint8_t status;
  uint16_t adcValue;
  uint16_t sensorMv;
  uint16_t averageMv;
  uint16_t sampleValue;
  uint32_t onTimeSeconds;
  bool isWarn;
  bool isAlarm;
} Mi80_106_SensorStatusInfo;

typedef enum Mi80_106_InterrruptFlags_enum {
  MI80_106_BUTTON1_RELEASE_INT_FLAG = 0x01,
  MI80_106_BUTTON1_PRESS_INT_FLAG = 0x02,
  MI80_106_BUTTON2_RELEASE_INT_FLAG = 0x04,
  MI80_106_BUTTON2_PRESS_INT_FLAG = 0x08,

  MI80_106_SENSOR_TURNED_ON_INT_FLAG = 0x10,
  MI80_106_SENSOR_TURNED_OFF_INT_FLAG = 0x20,
  MI80_106_SENSOR_READY_INT_FLAG = 0x40,
  MI80_106_SENSOR_SAMPLE_START_INT_FLAG = 0x80,
  MI80_106_SENSOR_SAMPLE_COMPLETE_INT_FLAG = 0x100,
  MI80_106_SENSOR_TIMEOUT_INT_FLAG = 0x200,

  MI80_106_IO1_INT_RISE_FLAG = 0x400,
  MI80_106_IO1_INT_FALL_FLAG = 0x800,
  MI80_106_IO2_INT_RISE_FLAG = 0x1000,
  MI80_106_IO2_INT_FALL_FLAG = 0x2000,

} Mi80_106_InterruptFlags;

#define MI80_106_DEFAULT_TIM2_FREQUENCY 50
#define MI80_106_SENSOR_SAMPLE_FREQUENCY DEFAULT_TIM2_FREQUENCY

typedef enum Mi80_106_LedPattern_enum {
  MI80_106_LED_PATTERN_OFF = 0,
  MI80_106_LED_PATTERN_WARMING_UP = 1,
  MI80_106_LED_PATTERN_READY = 2,
  MI80_106_LED_PATTERN_PASS = 3,
  MI80_106_LED_PATTERN_WARN = 4,
  MI80_106_LED_PATTERN_FAIL = 5
} Mi80_106_LedPattern;

typedef enum Mi80_106_SensorStatus_enum {
  MI80_106_SENSOR_STATUS_OFF = 0,
  MI80_106_SENSOR_STATUS_ON = 1,
  MI80_106_SENSOR_STATUS_WARMING_UP = 3,
  MI80_106_SENSOR_STATUS_READY = 5,
  MI80_106_SENSOR_STATUS_SAMPLE_STARTED = 9,
  MI80_106_SENSOR_STATUS_SAMPLE_COMPLETED = 11,
  MI80_106_SENSOR_STATUS_SAMPLE_RECOVERING = 21
} Mi80_106_SensorStatus;

typedef enum Mi80_106_Register_enum {
  MI80_106_REG_SENSOR_STATUS = 0x00,        // ro
  MI80_106_REG_SENSOR_ENABLE = 0x01,        // rw
  MI80_106_REG_SENSOR_CONFIG = 0x02,        // rw
  MI80_106_REG_SENSOR_SAMPLE_START = 0x03,  // wo
  MI80_106_REG_SENSOR_SAMPLE_VALUE = 0x04,  // ro
} Mi80_106_Register;

#define MI80_106_REG_SENSOR_STATUS_DEF \
  { MI80_106_REG_SENSOR_STATUS, I2C_DATA_TYPE_UINT8, sizeof(Mi80_106_SensorStatusInfo), MI80_REG_RO }
#define MI80_106_REG_SENSOR_ENABLE_DEF \
  { MI80_106_REG_SENSOR_SAMPLE_START, I2C_DATA_TYPE_BOOL, 1, MI80_REG_RW }
#define MI80_106_REG_SENSOR_CONFIG_DEF \
  { MI80_106_REG_SENSOR_CONFIG, I2C_DATA_TYPE_UINT8, sizeof(Mi80_106_SensorConfig), MI80_REG_RW }
#define MI80_106_REG_SENSOR_SAMPLE_START_DEF \
  { MI80_106_REG_SENSOR_SAMPLE_START, I2C_DATA_TYPE_BOOL, 1, MI80_REG_WO }
#define MI80_106_REG_SENSOR_SAMPLE_VALUE_DEF \
  { MI80_106_REG_SENSOR_SAMPLE_VALUE, I2C_DATA_TYPE_UINT8, 1, MI80_REG_RO }

#define MI80_106_REG_SENSOR_STATUS_INFO \
  { "sensorStatus", "Sensor Status", "Current state of sensor", MI80_106_REG_SENSOR_STATUS_DEF }
#define MI80_106_REG_SENSOR_ENABLE_INFO \
  { "sensorEnable", "Sensor Enable", "Turn the sensor on or off", MI80_106_REG_SENSOR_ENABLE_DEF }
#define MI80_106_REG_SENSOR_CONFIG_INFO \
  { "sensorConfig", "Sensor Config", "Get or set configuration", MI80_106_REG_SENSOR_CONFIG_DEF }
#define MI80_106_REG_SENSOR_SAMPLE_START_INFO \
  { "startSample", "Start Sample", "Start the sample process", MI80_106_REG_SENSOR_SAMPLE_START_DEF }
#define MI80_106_REG_SENSOR_SAMPLE_VALUE_INFO \
  { "sampleValue", "Sample Value", "Sample value in percent of low to high threshold", MI80_106_REG_SENSOR_SAMPLE_VALUE_DEF }

#define MI80_106_REG_MODULE_DESCRIPTION_DEF \
  { MI80_REG_MODULE_DESCRIPTION, I2C_DATA_TYPE_STRING, sizeof(MI80_106_MODULE_DESCRIPTION), MI80_REG_RO }
#define MI80_REG_106_MODULE_DESCRIPTION_INFO \
  { "description", "Description", "Module description", MI80_106_REG_MODULE_DESCRIPTION_DEF }

#define MI80_106_I2C_REG_INFO                       \
  {                                                 \
    MI80_106_REG_SENSOR_STATUS_INFO,           /**/ \
        MI80_106_REG_SENSOR_ENABLE_INFO,       /**/ \
        MI80_106_REG_SENSOR_CONFIG_INFO,       /**/ \
        MI80_106_REG_SENSOR_SAMPLE_START_INFO, /**/ \
        MI80_106_REG_SENSOR_SAMPLE_VALUE_INFO, /**/ \
        MI80_REG_I2C_ADRRESS_INFO,             /**/ \
        MI80_REG_MODULE_ID_INFO,               /**/ \
        MI80_REG_MODULE_INFO_INFO,             /**/ \
        MI80_REG_106_MODULE_DESCRIPTION_INFO,  /**/ \
        MI80_REG_INT_ENABLE_INFO,              /**/ \
        MI80_REG_INT_FLAGS_INFO,               /**/ \
        MI80_CMD_RESET_INFO,                   /**/ \
        MI80_CMD_PERSIST_CONFIG_INFO,          /**/ \
        MI80_REG_IO_1_MODE_INFO,               /**/ \
        MI80_REG_IO_2_MODE_INFO,               /**/ \
        MI80_REG_IO_1_INFO,                    /**/ \
        MI80_REG_IO_2_INFO,                    /**/ \
        {NULL},                                /**/ \
  }

#define MI80_106_INTERRUPT_INFO                                                                                            \
  {                                                                                                                        \
    {MI80_106_BUTTON1_PRESS_INT_FLAG, "button1Press", "Button 1 Press", "Button 1 was pressed"},                      /**/ \
        {MI80_106_BUTTON1_RELEASE_INT_FLAG, "button1Release", "Button 1 Released", "Button 1 was released"},          /**/ \
        {MI80_106_BUTTON2_PRESS_INT_FLAG, "button2Press", "Button 2 Press", "Button 2 was pressed"},                  /**/ \
        {MI80_106_BUTTON2_RELEASE_INT_FLAG, "button2Release", "Button 2 Released", "Button 2 was released"},          /**/ \
        {MI80_106_SENSOR_TURNED_ON_INT_FLAG, "sensorOn", "Sensor On", "Sensor turned on"},                            /**/ \
        {MI80_106_SENSOR_TURNED_OFF_INT_FLAG, "sensorOff", "Sensor Off", "Sensor turned off"},                        /**/ \
        {MI80_106_SENSOR_READY_INT_FLAG, "sensorReady", "Sensor Ready", "Sensor warmed up and ready"},                /**/ \
        {MI80_106_SENSOR_SAMPLE_START_INT_FLAG, "sampleStart", "Sample Start", "Sensor sampling begin"},              /**/ \
        {MI80_106_SENSOR_SAMPLE_COMPLETE_INT_FLAG, "sampleComplete", "Sample Complete", "Sensor sampling completed"}, /**/ \
        {MI80_106_IO1_INT_RISE_FLAG, "io1Rise", "IO1 Rise", "Rising edge on IO1"},                                    /**/ \
        {MI80_106_IO1_INT_FALL_FLAG, "io1Fall", "IO1 Fall", "Falling edge on IO1"},                                   /**/ \
        {MI80_106_IO2_INT_RISE_FLAG, "io2Rise", "IO2 Rise", "Rising edge on IO2"},                                    /**/ \
        {MI80_106_IO2_INT_FALL_FLAG, "io2Fall", "IO2 Fall", "Falling edge on IO2"},                                   /**/ \
        {0},                                                                                                          /**/ \
  }

#define MI80_106_I2C_MODULE_INFO \
  { MI80_106_DEFAULT_I2C_ADDRESS, 106, "Mi80-106", "Mi80-106", "Mi80-106 Alcohol sensor", MI80_106_I2C_REG_INFO, MI80_106_INTERRUPT_INFO }

#endif
