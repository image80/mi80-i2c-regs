#pragma once

#include "mi80_common_regs.h"

#define MI80_103_DEFAULT_I2C_ADDRESS 0x43
#define MI80_103_MODULE_ID 103
#define MI80_103_MODULE_NAME "Mi80-103"
#define MI80_103_MODULE_FEATURES HAS_STATUS_LED
#define MI80_103_MODULE_DESCRIPTION "Mi80-103 Mosfet Driver"

#define MI80_103_NMBR_RGB_LEDS 0
#define MI80_103_NMBR_BUTTONS 0
#define MI80_103_NMBR_IO 4

typedef struct Mi80_103_DriverInfo_struct {
  uint16_t status;
  uint16_t frequency;
  uint16_t dutyCycle;
  uint16_t vOnMv;
  uint16_t vOffMv;
  uint16_t vCurrentSenseMv;
  uint16_t currentWarnThresholdMa;
  uint16_t currentShutdownThresholdMa;
  uint16_t currentMa;
  uint16_t currentAvgMa;
} Mi80_103_DriverInfo;

typedef enum Mi80_103_Int_Flags_enum {
  MI80_103_IO1_INT_RISE_FLAG = 0x01,
  MI80_103_IO1_INT_FALL_FLAG = 0x02,
  MI80_103_IO2_INT_RISE_FLAG = 0x04,
  MI80_103_IO2_INT_FALL_FLAG = 0x08,
  MI80_103_IO3_INT_RISE_FLAG = 0x10,
  MI80_103_IO3_INT_FALL_FLAG = 0x20,
  MI80_103_IO4_INT_RISE_FLAG = 0x40,
  MI80_103_IO4_INT_FALL_FLAG = 0x80,
  MI80_103_INT_CURRENT_WARN_FLAG = 0x100,
  MI80_103_INT_CURRENT_SHUTDOWN_FLAG = 0x200,
} Mi80_103_Int_Flags;

typedef enum Mi80_103_Register_enum {
  MI80_103_REG_DRV_INFO = 0x00,
  MI80_103_REG_DRV_FREQ = 0x01,
  MI80_103_REG_DRV_DUTY_CYCLE = 0x02,
  MI80_103_REG_ADC_ON_VAL = 0x03,
  MI80_103_REG_ADC_OFF_VAL = 0x04,
  MI80_103_REG_ADC_SHUNT_VAL = 0x05,
  MI80_103_REG_DRV_ON_MV = 0x06,
  MI80_103_REG_DRV_OFF_MV = 0x07,
  MI80_103_REG_DRV_SHUNT_MV = 0x08,
  MI80_103_REG_DRV_CURRENT_MA = 0x09,
  MI80_103_REG_DRV_WARN_CURRENT_MA = 0x0A,
  MI80_103_REG_DRV_SHUTDOWN_CURRENT_MA = 0x0B,
  MI80_103_REG_DRV_ENABLE = 0x0C,

} Mi80_103_Register;

#define MI80_103_REG_DRV_INFO_DEF \
  { MI80_103_REG_DRV_INFO, I2C_DATA_TYPE_UINT16, 9, MI80_REG_RO }
#define MI80_103_REG_DRV_FREQ_DEF \
  { MI80_103_REG_DRV_FREQ, I2C_DATA_TYPE_UINT16, 1, MI80_REG_RW }
#define MI80_103_REG_DRV_DUTY_CYCLE_DEF \
  { MI80_103_REG_DRV_DUTY_CYCLE, I2C_DATA_TYPE_UINT8, 1, MI80_REG_RW }
#define MI80_103_REG_ADC_ON_VAL_DEF \
  { MI80_103_REG_ADC_ON_VAL, I2C_DATA_TYPE_UINT16, 1, MI80_REG_RO }
#define MI80_103_REG_ADC_OFF_VAL_DEF \
  { MI80_103_REG_ADC_ON_VAL, I2C_DATA_TYPE_UINT16, 1, MI80_REG_RO }
#define MI80_103_REG_ADC_SHUNT_VAL_DEF \
  { MI80_103_REG_ADC_SHUNT_VAL, I2C_DATA_TYPE_UINT16, 1, MI80_REG_RO }
#define MI80_103_REG_DRV_ON_MV_DEF \
  { MI80_103_REG_DRV_ON_MV, I2C_DATA_TYPE_UINT16, 1, MI80_REG_RO }
#define MI80_103_REG_DRV_OFF_MV_DEF \
  { MI80_103_REG_DRV_OFF_MV, I2C_DATA_TYPE_UINT16, 1, MI80_REG_RO }
#define MI80_103_REG_DRV_SHUNT_MV_DEF \
  { MI80_103_REG_DRV_SHUNT_MV, I2C_DATA_TYPE_UINT16, 1, MI80_REG_RO }
#define MI80_103_REG_DRV_CURRENT_MA_DEF \
  { MI80_103_REG_DRV_CURRENT_MA, I2C_DATA_TYPE_UINT16, 1, MI80_REG_RO }
#define MI80_103_REG_DRV_WARN_CURRENT_MA_DEF \
  { MI80_103_REG_DRV_WARN_CURRENT_MA, I2C_DATA_TYPE_UINT16, 1, MI80_REG_RW }
#define MI80_103_REG_DRV_SHUTDOWN_CURRENT_MA_DEF \
  { MI80_103_REG_DRV_SHUTDOWN_CURRENT_MA, I2C_DATA_TYPE_UINT16, 1, MI80_REG_RW }
#define MI80_103_REG_DRV_ENABLE_DEF \
  { MI80_103_REG_DRV_ENABLE, I2C_DATA_TYPE_UINT8, 1, MI80_REG_RW }

#define MI80_103_REG_DRV_INFO_INFO \
  { "drvInfo", "Driver Info", "Get driver config and voltage/current readings", MI80_103_REG_DRV_INFO_DEF }
#define MI80_103_REG_DRV_FREQ_INFO \
  { "drvFreq", "Driver Frequency", "Get or set driver frequency", MI80_103_REG_DRV_FREQ_DEF }
#define MI80_103_REG_DRV_DUTY_CYCLE_INFO \
  { "drvDuty", "Driver Duty Cycle", "Get or set driver duty cycle", MI80_103_REG_DRV_DUTY_CYCLE_DEF }
#define MI80_103_REG_ADC_ON_VAL_INFO \
  { "drvAdcOn", "Driver ON ADC", "Get driver ON ADC value", MI80_103_REG_ADC_ON_VAL_DEF }
#define MI80_103_REG_ADC_OFF_VAL_INFO \
  { "drvAdcOff", "Driver OFF ADC", "Get driver OFF ADC value", MI80_103_REG_ADC_OFF_VAL_DEF }
#define MI80_103_REG_ADC_SHUNT_VAL_INFO \
  { "currentSenseMv", "Shunt ADC", "Get shunt ADC value", MI80_103_REG_ADC_SHUNT_VAL_DEF }
#define MI80_103_REG_DRV_ON_MV_INFO \
  { "drvMvOn", "Driver ON MV", "Get driver ON voltage in MV", MI80_103_REG_DRV_ON_MV_DEF }
#define MI80_103_REG_DRV_OFF_MV_INFO \
  { "drvMvOff", "Driver OFF MV", "Get driver OFF voltage in MV", MI80_103_REG_DRV_OFF_MV_DEF }
#define MI80_103_REG_DRV_SHUNT_MV_INFO \
  { "shuntMv", "Shunt MV", "Get shunt voltage in MV (vSense * 50)", MI80_103_REG_DRV_SHUNT_MV_DEF }
#define MI80_103_REG_DRV_CURRENT_MA_INFO \
  { "currentMa", "Current MA", "Average current (ma ON * (dutyCycle /100))", MI80_103_REG_DRV_CURRENT_MA_DEF }
#define MI80_103_REG_DRV_WARN_CURRENT_MA_INFO \
  { "currentWarnMa", "Current Warn MA", "Current warning threshold", MI80_103_REG_DRV_WARN_CURRENT_MA_DEF }
#define MI80_103_REG_DRV_SHUTDOWN_CURRENT_MA_INFO \
  { "currentShutdownMa", "Current Shutdown MA", "Current shutdown threshold", MI80_103_REG_DRV_SHUTDOWN_CURRENT_MA_DEF }
#define MI80_103_REG_DRV_ENABLE_INFO \
  { "drvEnable", "Driver Enable", "Enable driver output", MI80_103_REG_DRV_ENABLE_DEF }

///////////////////////////////////////////////////////////////////////////////////////////

#define MI80_103_REGS_I2C_INFO                          \
  {                                                     \
    MI80_103_REG_DRV_INFO_INFO,                    /**/ \
        MI80_103_REG_DRV_FREQ_INFO,                /**/ \
        MI80_103_REG_DRV_DUTY_CYCLE_INFO,          /**/ \
        MI80_103_REG_ADC_ON_VAL_INFO,              /**/ \
        MI80_103_REG_ADC_OFF_VAL_INFO,             /**/ \
        MI80_103_REG_ADC_SHUNT_VAL_INFO,           /**/ \
        MI80_103_REG_DRV_ON_MV_INFO,               /**/ \
        MI80_103_REG_DRV_OFF_MV_INFO,              /**/ \
        MI80_103_REG_DRV_SHUNT_MV_INFO,            /**/ \
        MI80_103_REG_DRV_CURRENT_MA_INFO,          /**/ \
        MI80_103_REG_DRV_WARN_CURRENT_MA_INFO,     /**/ \
        MI80_103_REG_DRV_SHUTDOWN_CURRENT_MA_INFO, /**/ \
        MI80_103_REG_DRV_ENABLE_INFO,              /**/ \
        MI80_REG_IO_1_MODE_INFO,                   /**/ \
        MI80_REG_IO_2_MODE_INFO,                   /**/ \
        MI80_REG_IO_3_MODE_INFO,                   /**/ \
        MI80_REG_IO_4_MODE_INFO,                   /**/ \
        MI80_REG_IO_1_INFO,                        /**/ \
        MI80_REG_IO_2_INFO,                        /**/ \
        MI80_REG_IO_3_INFO,                        /**/ \
        MI80_REG_IO_4_INFO,                        /**/ \
        MI80_REG_STATUS_INFO,                      /**/ \
        MI80_REG_I2C_ADRRESS_INFO,                 /**/ \
        MI80_REG_MODULE_ID_INFO,                   /**/ \
        MI80_REG_MODULE_INFO_INFO,                 /**/ \
        MI80_REG_MODULE_DESCRIPTION_INFO,          /**/ \
        MI80_REG_INT_ENABLE_INFO,                  /**/ \
        MI80_REG_INT_FLAGS_INFO,                   /**/ \
        MI80_CMD_RESET_INFO,                       /**/ \
        MI80_CMD_PERSIST_CONFIG_INFO,              /**/ \
        MI80_CMD_PERSIST_ERASE_INFO,               /**/ \
        MI80_REG_SINT_GPIO_INFO,                   /**/ \
        MI80_REG_PWM1_FREQ_INFO,                   /**/ \
        {0},                                       /**/ \
  }

#define MI80_103_INTERRUPT_INFO                                                                                                \
  {                                                                                                                            \
    {MI80_103_IO1_INT_RISE_FLAG, "io1Rise", "IO 1 Rise", "IO #1 rising edge"},                                            /**/ \
        {MI80_103_IO1_INT_FALL_FLAG, "io1Fall", "IO 1 Fall", "IO #1 falling edge"},                                       /**/ \
        {MI80_103_IO2_INT_RISE_FLAG, "io2Rise", "IO 2 Rise", "IO #2 rising edge"},                                        /**/ \
        {MI80_103_IO2_INT_FALL_FLAG, "io2Fall", "IO 2 Fall", "IO #2 falling edge"},                                       /**/ \
        {MI80_103_IO3_INT_RISE_FLAG, "io3Rise", "IO 3 Rise", "IO #3 rising edge"},                                        /**/ \
        {MI80_103_IO3_INT_FALL_FLAG, "io3Fall", "IO 3 Fall", "IO #3 falling edge"},                                       /**/ \
        {MI80_103_IO4_INT_RISE_FLAG, "io4Rise", "IO 4 Rise", "IO #4 rising edge"},                                        /**/ \
        {MI80_103_IO4_INT_FALL_FLAG, "io4Fall", "IO 4 Fall", "IO #4 falling edge"},                                       /**/ \
        {MI80_103_INT_CURRENT_WARN_FLAG, "currentWarn", "Current Warn", "Current exceeded warning threshold"},            /**/ \
        {MI80_103_INT_CURRENT_SHUTDOWN_FLAG, "currentShutdown", "Current Shutdown", "Excess current triggered shutdown"}, /**/ \
        {0},                                                                                                              /**/ \
  }

#define MI80_103_I2C_MODULE_INFO \
  { MI80_103_DEFAULT_I2C_ADDRESS, 103, "Mi80-103", "Mi80-103", "Mi80-103 Mosfet Driver", MI80_103_REGS_I2C_INFO, MI80_103_INTERRUPT_INFO }
